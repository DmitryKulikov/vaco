﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace VaCo.App
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }

        private int _batchSize;
        private string[] _urls;

        public Program()
        {
            this._batchSize = 6;
            this._urls = GetUrls();
        }

        private void Run()
        {
            while (true)
            {
                Console.WriteLine("Poll()");
                Poll(_urls, _batchSize).Wait();
                Thread.Sleep(2500);
            }
        }

        private async Task Poll(string[] urls, int batchSize)
        {
            var tasks = new List<Task>(batchSize);

            foreach (var u in urls)
            {
                tasks.Add(Poll(u));

                if (tasks.Count == batchSize)
                {
                    Console.WriteLine("Batch is full, wait()");

                    await Task.WhenAll(tasks);
                    tasks.Clear();
                }
            }

            await Task.WhenAll(tasks);
        }

        private Task Poll(string url)
        {
            Console.WriteLine(url);
            return Task.Delay(10);
        }

        private string[] GetUrls()
        {
            var len = 25;
            var result = new string[25];
            for (var i = 0; i < len; i++)
            {
                result[i] = string.Concat("u_", i.ToString());
            }
            return result;
        }
    }
}
