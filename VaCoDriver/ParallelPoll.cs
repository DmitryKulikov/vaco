﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using VaCo.App.Model;

namespace VaCo.App
{
    class ParallelPoll
    {
        ConcurrentQueue<ModbusParameter> cq;

        public void Run(ModbusParameter[] items)
        {
            cq = new ConcurrentQueue<ModbusParameter>(items);
            Parallel.Invoke(Poll, Poll, Poll, Poll);
        }

        private void Poll()
        {
            ModbusParameter mp;
            while (cq.TryDequeue(out mp))
            {
            }
        }
    }
}
