﻿using System.Threading.Tasks;
using System.Xml;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public class OpcuaDeviceReader
    {
        private readonly string path;

        private OpcuaDevice plc;

        public OpcuaDeviceReader(string path)
        {
            this.path = path;
        }

        public OpcuaDevice Read()
        {
            plc = new OpcuaDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (reader.Read())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        public async Task<OpcuaDevice> ReadAsync()
        {
            plc = new OpcuaDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (await reader.ReadAsync())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        private void ReadNode(XmlReader reader)
        {
            if(reader.NodeType != XmlNodeType.Element)
            {
                return;
            }

            var tag = reader.Name;

            if (tag.Is("plc"))
            {
                ReadPlc(reader);
            }

            else if (tag.Is("commSettings"))
            {
                ReadCommSettings(reader);
            }
        }

        private void ReadPlc(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("name"))
                {
                    plc.Name = reader.Value;
                }
                else if (attr.Is("template"))
                {
                    //p.Address = reader.Value;
                }
            }
        }

        private void ReadCommSettings(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("namespace"))
                {
                    plc.Namespace = reader.Value;
                }
                else if (attr.Is("addrTCP"))
                {
                    plc.IP = reader.Value;
                }
                else if (attr.Is("addrHTTPS"))
                {
                    plc.Host = reader.Value;
                }
                else if (attr.Is("username"))
                {
                    plc.Username = reader.Value;
                }
                else if (attr.Is("password"))
                {
                    plc.Password = reader.Value;
                }
            }
        }
    }
}
