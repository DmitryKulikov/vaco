﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public class ModbusTemplateReader
    {
        private readonly string path;

        private string currentGroup;

        private string currentObject;

        private List<ModbusParameter> result;

        public ModbusTemplateReader(string path)
        {
            this.path = path;
        }

        public List<ModbusParameter> Read()
        {
            Reset();

            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (reader.Read())
                {
                    ReadNode(reader);
                }
            }

            return result;
        }

        public async Task<List<ModbusParameter>> ReadAsync()
        {
            Reset();

            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (await reader.ReadAsync())
                {
                    ReadNode(reader);
                }
            }

            return result;
        }

        private void Reset()
        {
            currentGroup = null;
            currentObject = null;
            result = new List<ModbusParameter>(32);
        }

        private void ReadNode(XmlReader reader)
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    ReadElem(reader);
                    break;

                case XmlNodeType.EndElement:
                    Clean(reader);
                    break;
            }
        }

        private void Clean(XmlReader reader)
        {
            var tag = reader.Name;

            if (tag.Is("object"))
            {
                currentObject = null;
            }

            else if (tag.Is("group"))
            {
                currentGroup = null;
            }
        }

        private void ReadElem(XmlReader reader)
        {
            var tag = reader.Name;

            if (tag.Is("parameter"))
            {
                ModbusParameter p;
                ReadParameter(reader, out p);
                p.Group = currentGroup;
                p.Object = currentObject;
                result.Add(p);
            }

            else if (tag.Is("object"))
            {
                currentObject = ReadAttribute(reader, "type");
            }

            else if (tag.Is("group"))
            {
                currentGroup = ReadAttribute(reader, "name");
            }
        }

        private string ReadAttribute(XmlReader reader, string attr)
        {
            while (reader.MoveToNextAttribute())
            {
                if (reader.Name.Is(attr))
                {
                    return reader.Value;
                }
            }
            return null;
        }

        private void ReadParameter(XmlReader reader, out ModbusParameter tcp)
        {
            tcp = new ModbusParameter();

            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("name"))
                {
                    tcp.Name = reader.Value;
                }
                else if (attr.Is("address"))
                {
                    tcp.Address = reader.Value;
                }
                else if (attr.Is("type"))
                {
                    tcp.Type = reader.Value.ParseParameterType();
                }
                else if (attr.Is("access"))
                {
                    tcp.Access = reader.Value.ParseAccessType();
                }
                else if (attr.Is("function"))
                {
                    tcp.Function = reader.Value.ParseByte();
                }
                else if (attr.Is("description"))
                {
                    tcp.Description = reader.Value;
                }
                else if (attr.Is("unit"))
                {
                    tcp.Unit = reader.Value;
                }
                else if (attr.Is("byteOrder"))
                {
                    tcp.ByteOrder = reader.Value.ParseByteOrderType();
                }
            }
        }
    }
}
