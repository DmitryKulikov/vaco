﻿using System.Xml;

namespace VaCo.App.Helpers
{
    public static class XmlReaderHelper
    {
        private static readonly XmlReaderSettings settings = new XmlReaderSettings() { Async = true };

        public static XmlReader Create(string path)
        {
            return XmlReader.Create(path, XmlReaderHelper.settings);
        }

        public static string ReadAttribute(this XmlReader reader, string attr)
        {
            while (reader.MoveToNextAttribute())
            {
                if (reader.Name.Is(attr))
                {
                    return reader.Value;
                }
            }
            return null;
        }
    }
}
