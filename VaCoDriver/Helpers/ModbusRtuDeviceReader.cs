﻿using System.Threading.Tasks;
using System.Xml;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public class ModbusRtuDeviceReader
    {
        private readonly string path;

        private ModbusRtuDevice plc;

        public ModbusRtuDeviceReader(string path)
        {
            this.path = path;
        }

        public ModbusRtuDevice Read()
        {
            plc = new ModbusRtuDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (reader.Read())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        public async Task<ModbusRtuDevice> ReadAsync()
        {
            plc = new ModbusRtuDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (await reader.ReadAsync())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        private void ReadNode(XmlReader reader)
        {
            if(reader.NodeType != XmlNodeType.Element)
            {
                return;
            }

            var tag = reader.Name;

            if (tag.Is("plc"))
            {
                ReadPlc(reader);
            }

            else if (tag.Is("commSettings"))
            {
                ReadCommSettings(reader);
            }
        }

        private void ReadPlc(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("name"))
                {
                    plc.Name = reader.Value;
                }
                else if (attr.Is("template"))
                {
                    //p.Address = reader.Value;
                }
            }
        }

        private void ReadCommSettings(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("unitID"))
                {
                    plc.ID = reader.Value.ParseInt();
                }
                else if (attr.Is("port"))
                {
                    plc.Port = reader.Value.ParseComPort();
                }
                else if (attr.Is("reqPeriod"))
                {
                    plc.RequestPeriod = reader.Value.ParseInt();
                }
                else if (attr.Is("timeout"))
                {
                    plc.Timeout = reader.Value.ParseInt();
                }
                else if (attr.Is("baudrate"))
                {
                    plc.Boudrate = reader.Value.ParseInt();
                }
                else if (attr.Is("parity"))
                {
                    plc.Parity = reader.Value.ParseParity();
                }
                else if (attr.Is("databits"))
                {
                    plc.DataBits = reader.Value.ParseInt();
                }
                else if (attr.Is("stopbits"))
                {
                    plc.StopBits = reader.Value.ParseInt();
                }
            }
        }
    }
}
