﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public class OpcuaTemplateReader
    {
        private readonly string path;

        private string currentGroup;

        private string currentObject;

        private List<OpcuaParameter> result;

        public OpcuaTemplateReader(string path)
        {
            this.path = path;
        }

        public List<OpcuaParameter> Read()
        {
            Reset();

            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (reader.Read())
                {
                    ReadNode(reader);
                }
            }

            return result;
        }

        public async Task<List<OpcuaParameter>> ReadAsync()
        {
            Reset();

            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (await reader.ReadAsync())
                {
                    ReadNode(reader);
                }
            }

            return result;
        }

        private void Reset()
        {
            currentGroup = null;
            currentObject = null;
            result = new List<OpcuaParameter>(32);
        }

        private void ReadNode(XmlReader reader)
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    ReadElem(reader);
                    break;

                case XmlNodeType.EndElement:
                    Clean(reader);
                    break;
            }
        }

        private void Clean(XmlReader reader)
        {
            var tag = reader.Name;

            if (tag.Is("object"))
            {
                currentObject = null;
            }

            else if (tag.Is("group"))
            {
                currentGroup = null;
            }
        }

        private void ReadElem(XmlReader reader)
        {
            var tag = reader.Name;

            if (tag.Is("parameter"))
            {
                OpcuaParameter p;
                ReadParameter(reader, out p);
                p.Group = currentGroup;
                p.Object = currentObject;
                result.Add(p);
            }

            else if (tag.Is("object"))
            {
                currentObject = reader.ReadAttribute("type");
            }

            else if (tag.Is("group"))
            {
                currentGroup = reader.ReadAttribute("name");
            }
        }

        private void ReadParameter(XmlReader reader, out OpcuaParameter tcp)
        {
            tcp = new OpcuaParameter();

            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("name"))
                {
                    tcp.Name = reader.Value;
                }
                else if (attr.Is("identifier"))
                {
                    tcp.ID = reader.Value.ParseInt();
                }
                else if (attr.Is("type"))
                {
                    tcp.Type = reader.Value.ParseParameterType();
                }
                else if (attr.Is("access"))
                {
                    tcp.Access = reader.Value.ParseAccessType();
                }
                else if (attr.Is("description"))
                {
                    tcp.Description = reader.Value;
                }
                else if (attr.Is("unit"))
                {
                    tcp.Unit = reader.Value;
                }
            }
        }
    }
}
