﻿using System.Threading.Tasks;
using System.Xml;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public class ModbusTcpDeviceReader
    {
        private readonly string path;

        private ModbusTcpDevice plc;

        public ModbusTcpDeviceReader(string path)
        {
            this.path = path;
        }

        public ModbusTcpDevice Read()
        {
            plc = new ModbusTcpDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (reader.Read())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        public async Task<ModbusTcpDevice> ReadAsync()
        {
            plc = new ModbusTcpDevice();
            using (XmlReader reader = XmlReaderHelper.Create(path))
            {
                while (await reader.ReadAsync())
                {
                    ReadNode(reader);
                }
            }
            return plc;
        }

        private void ReadNode(XmlReader reader)
        {
            if(reader.NodeType != XmlNodeType.Element)
            {
                return;
            }

            var tag = reader.Name;

            if (tag.Is("plc"))
            {
                ReadPlc(reader);
            }

            else if (tag.Is("commSettings"))
            {
                ReadCommSettings(reader);
            }
        }

        private void ReadPlc(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("name"))
                {
                    plc.Name = reader.Value;
                }
                else if (attr.Is("template"))
                {
                    //p.Address = reader.Value;
                }
            }
        }

        private void ReadCommSettings(XmlReader reader)
        {
            while (reader.MoveToNextAttribute())
            {
                var attr = reader.Name;

                if (attr.Is("unitID"))
                {
                    plc.ID = reader.Value.ParseInt();
                }
                else if (attr.Is("IP"))
                {
                    plc.IP = reader.Value.ParseIPAddress();
                }
                else if (attr.Is("port"))
                {
                    plc.Port = reader.Value.ParseInt();
                }
                else if (attr.Is("reqPeriod"))
                {
                    plc.RequestPeriod = reader.Value.ParseInt();
                }
                else if (attr.Is("timeout"))
                {
                    plc.Timeout = reader.Value.ParseInt();
                }
            }
        }
    }
}
