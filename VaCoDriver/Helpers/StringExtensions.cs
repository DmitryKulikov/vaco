﻿using System;
using System.Net;
using VaCo.App.Model;

namespace VaCo.App.Helpers
{
    public static class StringExtensions
    {
        public static bool Is(this string s, string what)
        {
            return string.Compare(s, what, true) == 0;
        }

        public static byte ParseByte(this string s)
        {
            byte v;
            byte.TryParse(s, out v);
            return v;
        }

        public static int ParseInt(this string s)
        {
            int v;
            int.TryParse(s, out v);
            return v;
        }

        public static double ParseDouble(this string s)
        {
            double v;
            double.TryParse(s, out v);
            return v;
        }

        public static IPAddress ParseIPAddress(this string s)
        {
            IPAddress v;
            IPAddress.TryParse(s, out v);
            return v;
        }

        public static AccessType ParseAccessType(this string s)
        {
            if (string.Compare(s, "R", true) == 0)
            {
                return AccessType.R;
            }
            else if (string.Compare(s, "RW", true) == 0)
            {
                return AccessType.RW;
            }
            else if (string.Compare(s, "WR", true) == 0)
            {
                return AccessType.RW;
            }
            //
            throw new ArgumentException("Invalid access type: " + s);
        }

        public static ParameterType ParseParameterType(this string s)
        {
            if (string.Compare(s, "bool", true) == 0)
            {
                return ParameterType.BOOL;
            }
            else if (string.Compare(s, "str", true) == 0)
            {
                return ParameterType.STR;
            }
            else if (string.Compare(s, "int16", true) == 0)
            {
                return ParameterType.INT16;
            }
            else if (string.Compare(s, "uint16", true) == 0)
            {
                return ParameterType.UINT16;
            }
            else if (string.Compare(s, "int32", true) == 0)
            {
                return ParameterType.INT32;
            }
            else if (string.Compare(s, "uint32", true) == 0)
            {
                return ParameterType.UINT32;
            }
            else if (string.Compare(s, "float32", true) == 0)
            {
                return ParameterType.FLOAT32;
            }
            else if (string.Compare(s, "float64", true) == 0)
            {
                return ParameterType.FLOAT64;
            }
            else if (string.Compare(s, "int64", true) == 0)
            {
                return ParameterType.INT64;
            }
            else if (string.Compare(s, "uint64", true) == 0)
            {
                return ParameterType.UINT64;
            }
            //
            throw new ArgumentException("Invalid parameter type: " + s);
        }

        public static ByteOrderType ParseByteOrderType(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return ByteOrderType.NONE;
            }
            if (string.Compare(s, "1x2", true) == 0)
            {
                return ByteOrderType._1x2;
            }
            else if (string.Compare(s, "2x1", true) == 0)
            {
                return ByteOrderType._2x1;
            }
            if (string.Compare(s, "1x2x3x4", true) == 0)
            {
                return ByteOrderType._1x2x3x4;
            }
            else if (string.Compare(s, "2x1x4x3", true) == 0)
            {
                return ByteOrderType._2x1x4x3;
            }
            else if (string.Compare(s, "3x4x1x2", true) == 0)
            {
                return ByteOrderType._3x4x1x2;
            }
            else if (string.Compare(s, "4x3x2x1", true) == 0)
            {
                return ByteOrderType._4x3x2x1;
            }
            //
            throw new ArgumentException("Invalid byte order type: " + s);
        }

        public static ComPort ParseComPort(this string s)
        {
            if (string.Compare(s, "COM1", true) == 0)
            {
                return ComPort.COM1;
            }
            else if (string.Compare(s, "COM2", true) == 0)
            {
                return ComPort.COM2;
            }
            else if (string.Compare(s, "COM3", true) == 0)
            {
                return ComPort.COM3;
            }
            //
            throw new ArgumentException("Invalid comport type: " + s);
        }

        public static Parity ParseParity(this string s)
        {
            if (string.Compare(s, "NONE", true) == 0)
            {
                return Parity.NONE;
            }
            else if (string.Compare(s, "EVEN", true) == 0)
            {
                return Parity.EVEN;
            }
            else if (string.Compare(s, "ODD", true) == 0)
            {
                return Parity.ODD;
            }
            //
            throw new ArgumentException("Invalid parity type: " + s);
        }
    }
}
