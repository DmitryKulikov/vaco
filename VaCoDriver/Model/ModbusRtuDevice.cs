﻿namespace VaCo.App.Model
{
    public struct ModbusRtuDevice
    {
        public string Name { get; set; }

        //templates ....

        public int ID { get; set; }

        public ComPort Port { get; set; }

        public int RequestPeriod { get; set; }

        public int Timeout { get; set; }

        public int Boudrate { get; set; }

        public Parity Parity { get; set; }

        public int DataBits { get; set; }

        public int StopBits { get; set; }
    }
}
