﻿namespace VaCo.App.Model
{
    public enum AccessType : byte
    {
        R, 
        RW
    }
}
