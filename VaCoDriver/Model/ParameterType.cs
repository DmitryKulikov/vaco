﻿namespace VaCo.App.Model
{
    public enum ParameterType : byte
    {
        BOOL,

        STR,

        INT16,

        UINT16,

        INT32,

        UINT32,

        FLOAT32,

        FLOAT64,

        INT64,

        UINT64
    }
}
