﻿using System.Net;

namespace VaCo.App.Model
{
    public struct ModbusTcpDevice
    {
        public string Name { get; set; }

        //templates ....

        public int ID { get; set; }

        public IPAddress IP { get; set; }

        public int Port { get; set; }

        public int RequestPeriod { get; set; }

        public int Timeout { get; set; }
    }
}
