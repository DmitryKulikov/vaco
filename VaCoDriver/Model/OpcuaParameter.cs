﻿namespace VaCo.App.Model
{
    public struct OpcuaParameter
    {
        public string Name { get; set; }

        public string Group { get; set; }

        public string Object { get; set; }

        public string Description { get; set; }

        public string Unit { get; set; }

        public int ID { get; set; }

        public AccessType Access { get; set; }

        public ParameterType Type { get; set; }
    }
}
