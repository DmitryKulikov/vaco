﻿namespace VaCo.App.Model
{
    public struct OpcuaDevice
    {
        public string Name { get; set; }

        public string Namespace { get; set; }

        public string IP { get; set; }

        public string Host { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
