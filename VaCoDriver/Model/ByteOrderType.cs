﻿namespace VaCo.App.Model
{
    public enum ByteOrderType: byte
    {
        NONE,

        _1x2,

        _2x1,

        _1x2x3x4,

        _2x1x4x3,

        _3x4x1x2,

        _4x3x2x1
    }
}
