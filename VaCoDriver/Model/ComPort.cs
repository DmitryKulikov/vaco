﻿namespace VaCo.App.Model
{
    public enum ComPort : byte
    {
        COM1,
        COM2,
        COM3
    }
}
