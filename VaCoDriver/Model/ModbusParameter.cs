﻿namespace VaCo.App.Model
{
    public struct ModbusParameter
    {
        public string Name { get; set; }

        public string Group { get; set; }

        public string Object { get; set; }

        public string Description { get; set; }

        public string Unit { get; set; }

        public string Address { get; set; }

        public AccessType Access { get; set; }

        public ParameterType Type { get; set; }

        public byte Function { get; set; }

        public ByteOrderType ByteOrder { get; set; }
    }
}
