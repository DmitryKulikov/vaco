﻿namespace VaCo.App.Model
{
    public enum Parity: byte
    {
        NONE,
        EVEN,
        ODD
    }
}
