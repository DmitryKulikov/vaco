﻿using System.Collections.Generic;
using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusRtuTemplateReaderTest
    {
        [Fact]
        public void ParseParamCountTest()
        {
            var items = Read("mod-rtu1.xml");
            Assert.Equal(18, items.Count);
        }

        [Fact]
        public void ParseParamTest()
        {
            var items = Read("mod-rtu1.xml");

            var cfg = items[11];
            Assert.Equal("g1", cfg.Group);
            Assert.Equal("Te", cfg.Object);
            Assert.Equal("var5", cfg.Name);
            Assert.Equal("10", cfg.Address);
            Assert.Equal(ParameterType.FLOAT32, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(2, cfg.Function);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
            Assert.Equal(ByteOrderType._3x4x1x2, cfg.ByteOrder);
            //
            cfg = items[3];
            Assert.Equal("g-pump", cfg.Group);
            Assert.Equal("Pump", cfg.Object);
            Assert.Equal("p4", cfg.Name);
            Assert.Equal("10.1", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(1, cfg.Function);
            Assert.Equal("******Нет описания", cfg.Description);
            Assert.Equal("А", cfg.Unit);
            Assert.Equal(ByteOrderType.NONE, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_Non_Object_ParamTest()
        {
            var items = Read("mod-rtu1.xml");

            var cfg = items[4];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("var5", cfg.Name);
            Assert.Equal("16.6", cfg.Address);
            Assert.Equal(ParameterType.UINT64, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(16, cfg.Function);
            Assert.Equal("//***---", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
            Assert.Equal(ByteOrderType._4x3x2x1, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_ParamTest()
        {
            var items = Read("mod-rtu1.xml");

            var cfg = items[16];
            Assert.Null(cfg.Group);
            Assert.Equal("non-group-obj", cfg.Object);
            Assert.Equal("p17", cfg.Name);
            Assert.Equal("36.6", cfg.Address);
            Assert.Equal(ParameterType.STR, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(5, cfg.Function);
            Assert.Equal("no no", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
            Assert.Equal(ByteOrderType._1x2, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Group_Non_Object_ParamTest()
        {
            var items = Read("mod-rtu1.xml");

            var cfg = items[2];
            Assert.Equal("g-pump", cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("P3", cfg.Name);
            Assert.Equal("10", cfg.Address);
            Assert.Equal(ParameterType.FLOAT32, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(3, cfg.Function);
            Assert.Equal("НЕТ ОПИСАНИЯ", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
            Assert.Equal(ByteOrderType._3x4x1x2, cfg.ByteOrder);
        }

        private List<ModbusParameter> Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS RTU", fn);
            return new ModbusTemplateReader(fn).Read();
        }
    }
}
