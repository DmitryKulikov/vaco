﻿using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusRtuDeviceReaderTest
    {
        [Fact]
        public void ReadPlcTest()
        {
            var cfg = Read("plc-mod-rtu1.xml");

            Assert.Equal("123-xxx", cfg.Name);
            Assert.Equal(132, cfg.ID);
            Assert.Equal(ComPort.COM2, cfg.Port);
            Assert.Equal(50, cfg.RequestPeriod);
            Assert.Equal(200, cfg.Timeout);
            Assert.Equal(1200, cfg.Boudrate);
            Assert.Equal(Parity.EVEN, cfg.Parity);
            Assert.Equal(7, cfg.DataBits);
            Assert.Equal(2, cfg.StopBits);
        }

        [Fact]
        public void ReadPlcTest2()
        {
            var cfg = Read("plc-mod-rtu2.xml");

            Assert.Equal("123-xxx", cfg.Name);
            Assert.Equal(256, cfg.ID);
            Assert.Equal(ComPort.COM3, cfg.Port);
            Assert.Equal(50, cfg.RequestPeriod);
            Assert.Equal(200, cfg.Timeout);
            Assert.Equal(1200, cfg.Boudrate);
            Assert.Equal(Parity.NONE, cfg.Parity);
            Assert.Equal(7, cfg.DataBits);
            Assert.Equal(2, cfg.StopBits);
        }

        private ModbusRtuDevice Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS RTU", fn);
            return new ModbusRtuDeviceReader(fn).Read();
        }
    }
}
