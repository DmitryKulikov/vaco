﻿using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusTcpGatewayDeviceReaderTest
    {
        [Fact]
        public void ReadPlcTest()
        {
            var cfg = Read("plc-mod-tcp-gateway1.xml");

            Assert.Equal("m_tcp_gateway", cfg.Name);
            Assert.Equal(454, cfg.ID);
            Assert.Equal("192.168.10.108", cfg.IP.ToString());
            Assert.Equal(1234, cfg.Port);
            Assert.Equal(500, cfg.RequestPeriod);
            Assert.Equal(200, cfg.Timeout);
        }

        [Fact]
        public void ReadPlcTest2()
        {
            var cfg = Read("plc-mod-tcp-gateway2.xml");

            Assert.Equal("M TCP GATEWAY**", cfg.Name);
            Assert.Equal(65, cfg.ID);
            Assert.Equal("192.168.11.178", cfg.IP.ToString());
            Assert.Equal(1234, cfg.Port);
            Assert.Equal(32, cfg.RequestPeriod);
            Assert.Equal(19, cfg.Timeout);
        }

        private ModbusTcpDevice Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS TCP GATEWAY", fn);
            return new ModbusTcpDeviceReader(fn).Read();
        }
    }
}
