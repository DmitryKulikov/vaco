﻿using System.Collections.Generic;
using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class OpcuaTemplateReaderTest
    {
        [Fact]
        public void ParseParamCountTest()
        {
            var items = Read("opcua-1.xml");
            Assert.Equal(8, items.Count);
        }

        [Fact]
        public void ParseParamTest()
        {
            var items = Read("opcua-1.xml");

            var cfg = items[6];
            Assert.Equal("g123", cfg.Group);
            Assert.Equal("NSX", cfg.Object);
            Assert.Equal("p4", cfg.Name);
            Assert.Equal(444, cfg.ID);
            Assert.Equal(ParameterType.UINT64, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("А", cfg.Unit);
            //
            cfg = items[3];
            Assert.Equal("g2", cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("p3", cfg.Name);
            Assert.Equal(44, cfg.ID);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal("_DESCRIPTION", cfg.Description);
            Assert.Equal("кПа", cfg.Unit);
        }

        [Fact]
        public void Parse_Non_Group_Non_Object_ParamTest()
        {
            var items = Read("opcua-1.xml");

            var cfg = items[7];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("non_g2", cfg.Name);
            Assert.Equal(0, cfg.ID);
            Assert.Equal(ParameterType.UINT64, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal("qwerty", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
        }

        [Fact]
        public void Parse_Non_Group_ParamTest()
        {
            var items = Read("opcua-1.xml");

            var cfg = items[0];
            Assert.Null(cfg.Group);
            Assert.Equal("Te", cfg.Object);
            Assert.Equal("o2", cfg.Name);
            Assert.Equal(666, cfg.ID);
            Assert.Equal(ParameterType.FLOAT64, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal("    ", cfg.Description);
            Assert.Equal("бар", cfg.Unit);
        }

        [Fact]
        public void Parse_Group_Non_Object_ParamTest()
        {
            var items = Read("opcua-1.xml");

            var cfg = items[3];
            Assert.Equal("g2", cfg.Group);
            Assert.Null(cfg.Object);
        }

        private List<OpcuaParameter> Read(string fn)
        {
            fn = Path.Combine("TestData", "OPCUA", fn);
            return new OpcuaTemplateReader(fn).Read();
        }
    }
}
