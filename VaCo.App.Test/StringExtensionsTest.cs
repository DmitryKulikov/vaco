﻿using System;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class StringExtensionsTest
    {
        [Theory]
        [InlineData("even", Parity.EVEN)]
        [InlineData("eVen", Parity.EVEN)]
        [InlineData("EVEN", Parity.EVEN)]
        [InlineData("Odd", Parity.ODD)]
        [InlineData("odd", Parity.ODD)]
        [InlineData("ODD", Parity.ODD)]
        [InlineData("nOne", Parity.NONE)]
        [InlineData("NONE", Parity.NONE)]
        [InlineData("none", Parity.NONE)]
        public void ParseParityTest(string input, Parity expected)
        {
            Assert.Equal(expected, input.ParseParity());
        }

        [Theory]
        [InlineData("_even", "Invalid parity type: _even")]
        [InlineData("xxx", "Invalid parity type: xxx")]
        [InlineData(" even", "Invalid parity type:  even")]
        [InlineData("1", "Invalid parity type: 1")]
        public void ParseParityErrorTest(string input, string expected)
        {
            try
            {
                input.ParseParity();
            }
            catch (ArgumentException e)
            {
                Assert.Equal(expected, e.Message);
            }
        }

        [Theory]
        [InlineData("com1", ComPort.COM1)]
        [InlineData("Com1", ComPort.COM1)]
        [InlineData("coM1", ComPort.COM1)]
        [InlineData("Com2",  ComPort.COM2)]
        [InlineData("COM2",  ComPort.COM2)]
        [InlineData("com2",  ComPort.COM2)]
        [InlineData("Com3", ComPort.COM3)]
        [InlineData("cOM3", ComPort.COM3)]
        [InlineData("com3", ComPort.COM3)]
        public void ParseComPortTest(string input, ComPort expected)
        {
            Assert.Equal(expected, input.ParseComPort());
        }

        [Theory]
        [InlineData("com", "Invalid comport type: com")]
        [InlineData("xxx", "Invalid comport type: xxx")]
        [InlineData(" even", "Invalid comport type:  even")]
        [InlineData("1", "Invalid comport type: 1")]
        public void ParseComPortErrorTest(string input, string expected)
        {
            try
            {
                input.ParseComPort();
            }
            catch (ArgumentException e)
            {
                Assert.Equal(expected, e.Message);
            }
        }

        [Theory]
        [InlineData("r", AccessType.R)]
        [InlineData("R", AccessType.R)]
        [InlineData("rw", AccessType.RW)]
        [InlineData("rW", AccessType.RW)]
        [InlineData("RW", AccessType.RW)]
        [InlineData("wr", AccessType.RW)]
        [InlineData("Wr", AccessType.RW)]
        [InlineData("Rw", AccessType.RW)]
        [InlineData("WR", AccessType.RW)]
        [InlineData("wR", AccessType.RW)]
        public void ParseAccessTypeTest(string input, AccessType expected)
        {
            Assert.Equal(expected, input.ParseAccessType());
        }

        [Theory]
        [InlineData("com", "Invalid access type: com")]
        [InlineData("xxx", "Invalid access type: xxx")]
        [InlineData(" even", "Invalid access type:  even")]
        [InlineData("1", "Invalid access type: 1")]
        public void ParseAccessTypeErrorTest(string input, string expected)
        {
            try
            {
                input.ParseAccessType();
            }
            catch (ArgumentException e)
            {
                Assert.Equal(expected, e.Message);
            }
        }

        [Theory]
        [InlineData("Bool", ParameterType.BOOL)]
        [InlineData("bool", ParameterType.BOOL)]
        [InlineData("BOOl", ParameterType.BOOL)]
        [InlineData("float32", ParameterType.FLOAT32)]
        [InlineData("Float32", ParameterType.FLOAT32)]
        [InlineData("FLOAT32", ParameterType.FLOAT32)]
        [InlineData("FLOAT64", ParameterType.FLOAT64)]
        [InlineData("float64", ParameterType.FLOAT64)]
        [InlineData("fLoat64", ParameterType.FLOAT64)]
        [InlineData("int16", ParameterType.INT16)]
        [InlineData("Int16", ParameterType.INT16)]
        [InlineData("INT16", ParameterType.INT16)]
        [InlineData("INT32", ParameterType.INT32)]
        [InlineData("inT32", ParameterType.INT32)]
        [InlineData("int32", ParameterType.INT32)]
        [InlineData("int64", ParameterType.INT64)]
        [InlineData("Int64", ParameterType.INT64)]
        [InlineData("iNt64", ParameterType.INT64)]
        [InlineData("str", ParameterType.STR)]
        [InlineData("Str", ParameterType.STR)]
        [InlineData("STR", ParameterType.STR)]
        [InlineData("uint16", ParameterType.UINT16)]
        [InlineData("uInt16", ParameterType.UINT16)]
        [InlineData("UINT16", ParameterType.UINT16)]
        [InlineData("uint32", ParameterType.UINT32)]
        [InlineData("Uint32", ParameterType.UINT32)]
        [InlineData("uInt32", ParameterType.UINT32)]
        [InlineData("uint64", ParameterType.UINT64)]
        [InlineData("UINT64", ParameterType.UINT64)]
        [InlineData("UiNt64", ParameterType.UINT64)]
        public void ParseParameterTypeTest(string input, ParameterType expected)
        {
            Assert.Equal(expected, input.ParseParameterType());
        }

        [Theory]
        [InlineData("com", "Invalid parameter type: com")]
        [InlineData("xxx", "Invalid parameter type: xxx")]
        [InlineData(" even", "Invalid parameter type:  even")]
        [InlineData("1", "Invalid parameter type: 1")]
        public void ParseParameterTypeErrorTest(string input, string expected)
        {
            try
            {
                input.ParseParameterType();
            }
            catch (ArgumentException e)
            {
                Assert.Equal(expected, e.Message);
            }
        }

        [Theory]
        [InlineData(null, ByteOrderType.NONE)]
        [InlineData("", ByteOrderType.NONE)]
        [InlineData("1x2", ByteOrderType._1x2)]
        [InlineData("1X2", ByteOrderType._1x2)]
        [InlineData("2x1", ByteOrderType._2x1)]
        [InlineData("2X1", ByteOrderType._2x1)]
        [InlineData("1x2x3x4", ByteOrderType._1x2x3x4)]
        [InlineData("1X2X3x4", ByteOrderType._1x2x3x4)]
        [InlineData("1X2X3X4", ByteOrderType._1x2x3x4)]
        [InlineData("2x1x4x3", ByteOrderType._2x1x4x3)]
        [InlineData("2X1X4X3", ByteOrderType._2x1x4x3)]
        [InlineData("2x1x4X3", ByteOrderType._2x1x4x3)]
        [InlineData("3x4x1x2", ByteOrderType._3x4x1x2)]
        [InlineData("3X4X1x2", ByteOrderType._3x4x1x2)]
        [InlineData("3x4X1X2", ByteOrderType._3x4x1x2)]
        [InlineData("4X3X2X1", ByteOrderType._4x3x2x1)]
        [InlineData("4x3x2x1", ByteOrderType._4x3x2x1)]
        [InlineData("4X3x2x1", ByteOrderType._4x3x2x1)]
        public void ParseByteOrderTypeTest(string input, ByteOrderType expected)
        {
            Assert.Equal(expected, input.ParseByteOrderType());
        }

        [Theory]
        [InlineData("com", "Invalid byte order type: com")]
        [InlineData("xxx", "Invalid byte order type: xxx")]
        [InlineData("   ", "Invalid byte order type:    ")]
        [InlineData("1", "Invalid byte order type: 1")]
        public void ParseByteOrderTypeErrorTest(string input, string expected)
        {
            try
            {
                input.ParseByteOrderType();
            }
            catch (ArgumentException e)
            {
                Assert.Equal(expected, e.Message);
            }
        }
    }
}
