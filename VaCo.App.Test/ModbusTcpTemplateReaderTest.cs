﻿using System.Collections.Generic;
using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusTcpTemplateReaderTest
    {
        [Fact]
        public void ParseParamCountTest()
        {
            var items = Read("ModbusTCPTemplate.xml");
            Assert.Equal(13, items.Count);
        }

        [Fact]
        public void ParseParamTest()
        {
            var items = Read("ModbusTCPTemplate.xml");

            var cfg = items[4];
            Assert.Equal("TempAirInRoom", cfg.Name);
            Assert.Equal("5", cfg.Address);
            Assert.Equal(ParameterType.FLOAT32, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(3, cfg.Function);
            Assert.Equal("------", cfg.Description);
            Assert.Equal("xxx", cfg.Unit);
            Assert.Equal(ByteOrderType._2x1x4x3, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_Non_Object_ParamTest()
        {
            var items = Read("ModbusTCPTemplate.xml");

            var cfg = items[12];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("p6", cfg.Name);
            Assert.Equal("111.3", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(5, cfg.Function);
            Assert.Equal("132 456 798", cfg.Description);
            Assert.Equal("кПа", cfg.Unit);
            Assert.Equal(ByteOrderType.NONE, cfg.ByteOrder);
            //

            cfg = items[0];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("p0", cfg.Name);
            Assert.Equal("111.3", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(5, cfg.Function);
            Assert.Equal("132 456 798", cfg.Description);
            Assert.Equal("кПа", cfg.Unit);
            Assert.Equal(ByteOrderType.NONE, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_ParamTest()
        {
            var items = Read("ModbusTCPTemplate.xml");

            var cfg = items[10];
            Assert.Null(cfg.Group);
            Assert.Equal("Te-2", cfg.Object);
            Assert.Equal("Te-2-param*", cfg.Name);
            Assert.Equal("999.666", cfg.Address);
            Assert.Equal(ParameterType.UINT64, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(4, cfg.Function);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("А", cfg.Unit);
            Assert.Equal(ByteOrderType._2x1, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Group_Non_Object_ParamTest()
        {
            var items = Read("ModbusTCPTemplate.xml");

            var cfg = items[5];
            Assert.Equal("Вентиляция", cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("VAR1", cfg.Name);
            Assert.Equal("12.1", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(3, cfg.Function);
            Assert.Equal("", cfg.Description);
            Assert.Equal("", cfg.Unit);
            Assert.Equal(ByteOrderType.NONE, cfg.ByteOrder);
        }

        private List<ModbusParameter> Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS TCP", fn);
            return new ModbusTemplateReader(fn).Read();
        }
    }
}
