﻿using System.Collections.Generic;
using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusTcpGatewayTemplateReaderTest
    {
        [Fact]
        public void ParseParamCountTest()
        {
            var items = Read("tcp-gateway-1.xml");
            Assert.Equal(11, items.Count);
        }

        [Fact]
        public void ParseParamTest()
        {
            var items = Read("tcp-gateway-1.xml");

            var cfg = items[4];
            Assert.Equal("group 2-2", cfg.Group);
            Assert.Equal("QF", cfg.Object);
            Assert.Equal("p_w_coil", cfg.Name);
            Assert.Equal("45.3", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(5, cfg.Function);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("В", cfg.Unit);
            Assert.Equal(ByteOrderType._1x2x3x4, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_Non_Object_ParamTest()
        {
            var items = Read("tcp-gateway-1.xml");

            var cfg = items[1];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("p_r_mult", cfg.Name);
            Assert.Equal("2", cfg.Address);
            Assert.Equal(ParameterType.INT16, cfg.Type);
            Assert.Equal(AccessType.R, cfg.Access);
            Assert.Equal(3, cfg.Function);
            Assert.Equal("132 456 798", cfg.Description);
            Assert.Equal("кВт", cfg.Unit);
            Assert.Equal(ByteOrderType._1x2, cfg.ByteOrder);
            //

            cfg = items[0];
            Assert.Null(cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("p_w_single", cfg.Name);
            Assert.Equal("45", cfg.Address);
            Assert.Equal(ParameterType.FLOAT32, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(6, cfg.Function);
            Assert.Equal("x y z", cfg.Description);
            Assert.Equal("А", cfg.Unit);
            Assert.Equal(ByteOrderType._1x2x3x4, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Non_Group_ParamTest()
        {
            var items = Read("tcp-gateway-1.xml");

            var cfg = items[2];
            Assert.Null(cfg.Group);
            Assert.Equal("Pump_ITP", cfg.Object);
            Assert.Equal("p_w_mult", cfg.Name);
            Assert.Equal("4", cfg.Address);
            Assert.Equal(ParameterType.STR, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(16, cfg.Function);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("°C", cfg.Unit);
            Assert.Equal(ByteOrderType.NONE, cfg.ByteOrder);
        }

        [Fact]
        public void Parse_Group_NonObject_ParamTest()
        {
            var items = Read("tcp-gateway-1.xml");

            var cfg = items[9];
            Assert.Equal("group zero", cfg.Group);
            Assert.Null(cfg.Object);
            Assert.Equal("group zero para2", cfg.Name);
            Assert.Equal("55.3", cfg.Address);
            Assert.Equal(ParameterType.BOOL, cfg.Type);
            Assert.Equal(AccessType.RW, cfg.Access);
            Assert.Equal(5, cfg.Function);
            Assert.Equal("Нет описания", cfg.Description);
            Assert.Equal("В", cfg.Unit);
            Assert.Equal(ByteOrderType._4x3x2x1, cfg.ByteOrder);
        }

        private List<ModbusParameter> Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS TCP GATEWAY", fn);
            return new ModbusTemplateReader(fn).Read();
        }
    }
}
