﻿using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class ModbusTcpDeviceReaderTest
    {
        [Fact]
        public void ReadPlcTest()
        {
            var cfg = Read("plc-mod-tcp1.xml");

            Assert.Equal("666", cfg.Name);
            Assert.Equal(777, cfg.ID);
            Assert.Equal("10.10.10.12", cfg.IP.ToString());
            Assert.Equal(666, cfg.Port);
            Assert.Equal(2000, cfg.RequestPeriod);
            Assert.Equal(5000, cfg.Timeout);
        }

        [Fact]
        public void ReadPlcTest2()
        {
            var cfg = Read("plc-mod-tcp2.xml");

            Assert.Equal("m_tcp_666", cfg.Name);
            Assert.Equal(11, cfg.ID);
            Assert.Equal("192.168.1.133", cfg.IP.ToString());
            Assert.Equal(5555, cfg.Port);
            Assert.Equal(5000, cfg.RequestPeriod);
            Assert.Equal(10000, cfg.Timeout);
        }

        private ModbusTcpDevice Read(string fn)
        {
            fn = Path.Combine("TestData", "MODBUS TCP", fn);
            return new ModbusTcpDeviceReader(fn).Read();
        }
    }
}
