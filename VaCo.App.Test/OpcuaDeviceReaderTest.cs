﻿using System.IO;
using VaCo.App.Helpers;
using VaCo.App.Model;
using Xunit;

namespace VaCo.App.Test
{
    public class OpcuaDeviceReaderTest
    {
        [Fact]
        public void ReadPlcTest()
        {
            var cfg = Read("plc-opcua1.xml");

            Assert.Equal("opc ua dev", cfg.Name);
            Assert.Equal("a namespace here", cfg.Namespace);
            Assert.Equal("opc.tcp://1.2.3.4", cfg.IP);
            Assert.Equal("https://www.web.com", cfg.Host);
            Assert.Equal("Me", cfg.Username);
            Assert.Equal("pswd", cfg.Password);
        }

        [Fact]
        public void ReadPlcTest2()
        {
            var cfg = Read("plc-opcua2.xml");

            Assert.Equal("A NEW opc ua dev", cfg.Name);
            Assert.Equal("123 456", cfg.Namespace);
            Assert.Equal("opc.tcp://10.198.20.164", cfg.IP);
            Assert.Equal("https://me.ru", cfg.Host);
            Assert.Equal("Lenin", cfg.Username);
            Assert.Equal("Vladimir", cfg.Password);
        }

        private OpcuaDevice Read(string fn)
        {
            fn = Path.Combine("TestData", "OPCUA", fn);
            return new OpcuaDeviceReader(fn).Read();
        }
    }
}
